import os
import re
import requests
from bs4 import BeautifulSoup
import pprint
import cssutils
import logging
from urllib.parse import urlparse


def createFolder(storePath):
    folderPath = os.getcwd() + "\\" + storePath
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)

#
# ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── I ──────────
#   :::::: S T O P P I N G   T H E   L O G G I N G   T H E   W A R N I N G S   A N D   E R R O R S : :  :   :    :     :        :          :
# ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
#

cssutils.log.setLevel(logging.CRITICAL)

#
# ─── END ────────────────────────────────────────────────────────────────────────
#

    

site = 'https://www.apple.com/in/iphone-xs'

urlpar = urlparse(site)

parsedUrl = urlpar.scheme + "://" + urlpar.netloc
urlpar = urlparse(site)


siteNetLoc = urlpar.netloc
folderName = siteNetLoc.split(".")
createFolder(folderName[1])
finalImageUrls = []
response = requests.get(site)

soup = BeautifulSoup(response.text, 'html.parser')

pp = pprint.PrettyPrinter(indent=4)

links = soup.find_all('link',{'rel':'stylesheet'})

urls = [link['href'] for link in links]

for url in urls:
    URL= parsedUrl + url
    posiOfSlashOfURL = [pos for pos, char in enumerate(URL) if char == "/"]
    extension = os.path.splitext(URL)[1]
    if extension == ".css" and extension != "":
        try:
            cssText = requests.get(URL)
            parsedCSS = cssutils.parseUrl(URL,encoding='utf-8')
            for rule in parsedCSS.cssRules:
                if rule.type == rule.STYLE_RULE:
                    for property in rule.style:
                        if property.name == 'background-image':
                            if "url(" in property.value:
                                l = len(property.value)
                                posiUrl = property.value.find('url(')
                                imgloc = property.value[posiUrl + 4 :l-1]
                                dataUriChecker = "data:image" in imgloc
                                if dataUriChecker:
                                    pass
                                else:
                                    slashPos = imgloc.find('/')
                                    if slashPos == 2:
                                        posiOfSlashOfImgLoc = [pos for pos, char in enumerate(imgloc) if char == "/"]
                                        noOfAccur = imgloc.count('../') + 1
                                        slicedImgLoc = imgloc[posiOfSlashOfImgLoc[noOfAccur - 2]:]
                                        slicedUrl = URL[: - (len(URL) - posiOfSlashOfURL[-noOfAccur])]
                                        finalImageLoc = slicedUrl + slicedImgLoc
                                        finalImageUrls.append(finalImageLoc)
                                        pp.pprint(finalImageLoc)
                                    if slashPos == 1:
                                        posiOfSlashOfImgLoc = [pos for pos, char in enumerate(imgloc) if char == "/"]
                                        slicedUrl = URL[: - (len(URL) - posiOfSlashOfURL[-1])]
                                        slicedImgLoc = imgloc[1:]
                                        finalImageLoc = slicedUrl + slicedImgLoc
                                        finalImageUrls.append(finalImageLoc)
                                        pp.pprint(finalImageLoc)
                                    if slashPos == 0:
                                        urlpar = urlparse(site)
                                        parsedUrl = urlpar.scheme + "://" + urlpar.netloc
                                        finalImageLoc = parsedUrl + imgloc
                                        finalImageUrls.append(finalImageLoc)
                                        pp.pprint(finalImageLoc)
        except:
            pass

for url in finalImageUrls:
    imageName = url.rsplit('/')[-1]
    imageData = requests.get(url).content
    with open("./" + folderName[1] +"/"+ imageName,'wb') as handler:
        handler.write(imageData)
        print("Saving..It may take a sec :)")
    
